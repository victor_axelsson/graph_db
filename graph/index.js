const fs = require('fs');

Unicorn = {}; 
Unicorn.Q = {}; 
Unicorn.G = {}; 
Unicorn.Pipetypes = {}

Unicorn.graph = function(V, E){
    var graph = Object.create(Unicorn.G)

    graph.edges = [];
    graph.vertices = []; 
    graph.verticeIndex = {}; 
    graph.autoid = 1;

    if(Array.isArray(V)){
        graph.addVertices(V); 
    }

    if(Array.isArray(E)){
        graph.addEdges(E); 
    }

    return graph; 
}

Unicorn.G.addVertex = function(vertex){
    if(!vertex._id){
        vertex._id = this.autoid++; 
    }else if(this.findVertexById(vertex._id)){
        return Unicorn.error('A vertex with that ID already exists'); 
    }

    this.vertices.push(vertex); 
    this.verticeIndex[vertex._id] = vertex; 
    vertex._out = [];
    vertex._in = []; 

    return vertex._id;  
}

Unicorn.G.addVertices = function(vs) {
    vs.forEach(this.addVertex.bind(this)); 
}

Unicorn.G.addEdges = function(es){
    es.forEach(this.addEdge.bind(this)); 
}

Unicorn.G.addEdge = function(edge){
    edge._in = this.findVertexById(edge._in); 
    edge._out = this.findVertexById(edge._out); 

    if(!(edge._in && edge._out)){
        return Unicorn.error(`That edge's ${edge._in ? 'out' : 'in'} vertex wasn't found`); 
    }

    edge._out._out.push(edge); 
    edge._in._in.push(edge); 
    this.edges.push(edge); 
}

Unicorn.Q.add = function(pipetype, args){
    var step = [pipetype, args]; 
    this.program.push(step); 
    return this; 
}

Unicorn.G.v = function(){
    var query = Unicorn.query(this); 
    query.add('vertex', [].slice.call(arguments)); 
    return query; 
}

Unicorn.error = function(message){
    console.log(message); 
    return false; 
} 

Unicorn.query = function(graph){
    var query = Object.create(Unicorn.Q)
    query.graph = graph; 
    query.state = []; 
    query.program = []; 
    query.gremlins = [];

    return query; 
}

Unicorn.addPipetype = function(name, fun){
    Unicorn.Pipetypes[name] = fun; 
    Unicorn.Q[name] = function(){
        return this.add(name, [].slice.apply(arguments)); 
    }
}

Unicorn.getPipetype = function(name){
    var pipetype = Unicorn.Pipetypes[name]; 

    if(!pipetype){
        Unicorn.error(`Unrecognized pipetype: ${name}`); 
    }

    return pipetype || Unicorn.fauxPipetype; 
}

Unicorn.fauxPipetype = function(_, _, maybe_gremlin){
    return maybe_gremlin || 'pull'; 
}

Unicorn.makeGremlin = (vertex, state) => {
    return { vertex, state: state || {} }; 
}

Unicorn.gotoVertex = function(gremlin, vertex){
    return Unicorn.makeGremlin(vertex, gremlin.state); 
}

Unicorn.G.findVertices = function(args){
    if(typeof args[0] == 'object'){
        return this.searchVertices(args[0]); 
    }else if(args.length == 0){
        return this.vertices.slice(); 
    }else{
        return this.findVericesByIds(args); 
    }
}

Unicorn.G.findVericesByIds = function(ids){
    if(ids.length == 1){
        var maybe_vertex = this.findVertexById(ids[0]); 
        return maybe_vertex ? [maybe_vertex] : []; 
    }

    return ids.map(this.findVertexById.bind(this)).filter(Boolean); 
}

Unicorn.G.findVertexById = function(vertex_id){
    return this.verticeIndex[vertex_id]; 
}

Unicorn.G.searchVertices = function(filter){
    return this.vertices.filter((vertex) => {
        return Unicorn.objectFilter(vertex, filter); 
    }); 
}

Unicorn.filterEdges = function(filter){
    return (edge) => {
        if(!filter){
            return true; 
        }

        if(typeof filter == 'string'){
            return edge._label == filter; 
        }

        if(Array.isArray(filter)){
            return !!~filter.indexOf(edge._label);
        }

        return Unicorn.objectFilter(edge, filter); 
    }; 
}

Unicorn.objectFilter = function(thing, filter) {
    for(var key in filter){
        if(thing[key] !== filter[key]){
                return false; 
        }
    }

    return true; 
}


Unicorn.addPipetype('vertex', function(graph, args, gremlin, state){
    if(!state.vertices){
        state.vertices = graph.findVertices(args);
    }

    if(!state.vertices.length){
        return 'done'; 
    }

    var vertex = state.vertices.pop(); 
    return Unicorn.makeGremlin(vertex, gremlin.state)
}); 

Unicorn.simpleTraversal = function(dir){
    var find_method = dir == 'out' ? 'findOutEdges': 'findInEdges'; 
    var edge_list = dir == 'out' ? '_in': '_out'; 

    return (graph, args, gremlin, state) => {
        if(!gremlin && (!state.edges || !state.edges.length)){
            return 'pull'; 
        }

        if(!state.edges || !state.edges.length){
            state.gremlin = gremlin; 
            state.edges = graph[find_method](gremlin.vertex).filter(Unicorn.filterEdges(args[0])); 
        }

        if(!state.edges.length) {
            return 'pull'; 
        }

        var vertex = state.edges.pop()[edge_list]; 
        return Unicorn.gotoVertex(state.gremlin, vertex); 
    }; 
}

Unicorn.addPipetype('out', Unicorn.simpleTraversal('out')); 
Unicorn.addPipetype('in', Unicorn.simpleTraversal('in')); 


Unicorn.addPipetype('property', function(graph, args, gremlin, state){
    if(!gremlin){
        return 'pull'; 
    }

    gremlin.result = gremlin.vertex[args[0]];

    return gremlin.result == null ? false : gremlin; 
}); 

Unicorn.addPipetype('relationOutOf', function(graph, args, gremlin, state){
    if(!state.vertices && !gremlin){
        return 'pull'
    } 

    if(!state.vertices){
        state.vertices = gremlin.vertex._out.filter((edge) => {
            return edge[args[0]] == args[1];  
        }).map((edge) => {
            return edge._in
        });
    }

    if(!state.vertices.length){
        return 'done'; 
    }

    var vertex = state.vertices.pop()
    return Unicorn.makeGremlin(vertex, gremlin.state)
}); 

Unicorn.addPipetype('unique', function(graph, args, gremlin, state){
    if(!gremlin){
        return 'pull'; 
    }

    if(state[gremlin.vertex._id]){
        return 'pull'; 
    }

    state[gremlin.vertex._id] = true;
    return gremlin; 
}); 

Unicorn.addPipetype('filter', function(graph, args, gremlin, state) {
    if(!gremlin){
        return 'pull'; 
    }

    if(typeof args[0] == 'object'){
        return Unicorn.objectFilter(gremlin.vertex, args[0]) ? gremlin : 'pull'; 
    }

    if(typeof args[0] != 'function') {
        Unicorn.error('Filter is not a function: ' + args[0])
        return gremlin; 
    }

    if(!args[0](gremlin.vertex, gremlin)){
        return 'pull'; 
    }

    return gremlin; 
}); 

Unicorn.addPipetype('take', function(graph, args, gremlin, state){
    state.taken = state.taken || 0; 

    if(state.taken == args[0]){
        state.taken = 0;
        return 'done'; 
    }

    if(!gremlin){
        return 'pull'; 
    }

    state.taken++; 
    return gremlin; 
}); 

Unicorn.addPipetype('as', function(graph, args, gremlin, state) {
    if(!gremlin){
        return 'pull'; 
    }

    gremlin.state.as = gremlin.state.as || {}; 
    gremlin.state.as[args[0]] = gremlin.vertex; 
    
    return gremlin; 
}); 

Unicorn.addPipetype('merge', function(graph, args, gremlin, state){
    if(!state.vertices && !gremlin){
        return 'pull'; 
    }

    if(!state.vertices || !state.vertices.length){
        var obj = (gremlin.state || {}).as || {}; 
        state.vertices = args.map((id) => {
            return obj[id]
        }).filter(Boolean); 
    }

    if(!state.vertices.length){
        return 'pull'; 
    }

    var vertex = state.vertices.pop(); 
    return Unicorn.makeGremlin(vertex, gremlin.state); 
}); 

Unicorn.addPipetype('except', function(graph, args, gremlin, state) {
    if(!gremlin){
        return 'pull'; 
    }

    if(gremlin.vertex == gremlin.state.as[args[0]]){
        return 'pull'; 
    }

    return gremlin; 
}); 

Unicorn.addPipetype('back', function(graph, args, gremlin, state){
    if(!state.vertices && !gremlin){
        return 'pull'; 
    }

    return Unicorn.gotoVertex(gremlin, gremlin.state.as[args[0]]); 
}); 


// Interpreter
Unicorn.Q.run = function(){
    this.program = Unicorn.transform(this.program); 

    var max = this.program.length -1;
    var maybe_gremlin = false; 
    var results = []; 
    var done = -1; 
    var pc = max; 
    var step, state, pipetype; 
    
    while(done < max){
        var ts = this.state; 
        step = this.program[pc]; 
        state = (ts[pc] = ts[pc] || {}); 
        pipetype = Unicorn.getPipetype(step[0]); 
        
        maybe_gremlin = pipetype(this.graph, step[1], maybe_gremlin, state);

        if(maybe_gremlin == 'pull'){
            maybe_gremlin = false; 
            if(pc-1 > done){
                pc--; 
                continue; 
            }else{
                done = pc; 
            }
        }

        if(maybe_gremlin == 'done'){
            maybe_gremlin = false; 
            done = pc; 
        }

        pc++; 
        
        if(pc > max){
            if(maybe_gremlin){
                results.push(maybe_gremlin); 
            }
            maybe_gremlin = false; 
            pc--; 
        }
    }

    // build the results
    results = results.map(function(gremlin) {
        return gremlin.result != null ? gremlin.result : gremlin.vertex; 
    }); 

    return results; 
}

// Query transformers

Unicorn.T = []; 

Unicorn.addTransformer = function(fun, priority) {
    if(typeof fun != 'function'){
        return Unicorn.error('Invalid transformer function'); 
    }

    for(var i = 0; i < Unicorn.T.length; i++){
        if(priority > Unicorn.T[i].priority){
            break;
        }
    }

    Unicorn.T.splice(i, 0, { priority, fun }); 
}

/*
Unicorn.Q.run = function(){
    this.program = Unicorn.transform(this.program); 
    console.log(this.program)
}
*/

Unicorn.transform = function(program){
    return Unicorn.T.reduce(function(acc, transformer){
        return transformer.fun(acc); 
    }, program); 
}

// Alias
Unicorn.addAlias = function(newname, oldname, defaults){
    defaults = defaults || []; 
    Unicorn.addTransformer((program) => {
        return program.map((step) => {
            if(step[0] != newname){
                return step; 
            }

            return [oldname, Unicorn.extend(step[1], defaults)]; 
        }); 
    }, 100); 

    Unicorn.addPipetype(newname, () => {}); 
}

Unicorn.extend = function(list, defaults){
    return Object.keys(defaults).reduce((acc, key) => {
        if(typeof list == 'undefined'){
            return acc; 
        }

        acc[key] = defaults[key]; 
        return acc; 
    }, list); 
}

Unicorn.addAlias('parents', 'out', ['parents']); 
Unicorn.addAlias('children', 'in', ['children']);

// Performance
/*
Unicorn.G.findInEdges = (vertex) => {
    return this.edges.filter((edge) => {
        return edge._in._id == vertex._id; 
    }); 
}

Unicorn.G.findOutEdges = (vertex) => {
    return this.edges.filter((edge) => {
        return edge._out._id == vertex._id; 
    }); 
}
*/

Unicorn.G.findInEdges  = function(vertex){ return vertex._in  }
Unicorn.G.findOutEdges = function(vertex){ return vertex._out }

// Serialization
Unicorn.jsonify = function(graph){
    return `{"V": ${JSON.stringify(graph.vertices, Unicorn.cleanVertex)}, "E": ${JSON.stringify(graph.edges, Unicorn.cleanEdge)}}`; 
}

Unicorn.cleanVertex = (key, value) => {
    return (key == '_in' || key == '_out') ? undefined : value; 
}

Unicorn.cleanEdge = (key, value) => {
    return (key == '_in' || key == '_out') ? value._id : value; 
}

Unicorn.G.toString = function(){
    return Unicorn.jsonify(this); 
}

Unicorn.fromString = (string) => {
    var obj = JSON.parse(string); 
    return Unicorn.graph(obj.V, obj.E); 
}

Unicorn.persist = (graph, name) => {
    name = name || 'graph'
    fs.writeFileSync(`UNICORN::${name}`, graph);
}

Unicorn.depersist = (name) => {
    name = 'UNICORN::' + (name || 'graph')
    var flatgraph = fs.readFileSync(name);
    return Unicorn.fromString(flatgraph)
}

module.exports = Unicorn; 