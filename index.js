let db = require('./graph'); 

let g = db.graph(); 

let lenaId = g.addVertex({firstName: "Lena", lastname: "Axelsson"})
let victorId = g.addVertex({firstName: "Victor", lastname: "Axelsson"})
let carlId = g.addVertex({firstName: "Carl", lastname: "Axelsson"})
let bertilId = g.addVertex({firstName: "Bertil", lastname: "Axelsson"})
let stigId = g.addVertex({firstName: "Stig", lastname: "Johansson"})
let sonjaId = g.addVertex({firstName: "Sonja", lastname: "Johansson"})

g.addEdge({
    _in: lenaId,
    _out: victorId,
    _type: "parentOf"
})

g.addEdge({
    _in: lenaId,
    _out: carlId,
    _type: "parentOf"
})

g.addEdge({
    _in: bertilId,
    _out: victorId,
    _type: "parentOf"
})

g.addEdge({
    _in: bertilId,
    _out: carlId,
    _type: "parentOf"
})

g.addEdge({
    _in: victorId,
    _out: carlId,
    _type: "sibling"
})

g.addEdge({
    _in: stigId,
    _out: lenaId,
    _type: "parentOf"
})

g.addEdge({
    _in: sonjaId,
    _out: lenaId,
    _type: "parentOf"
})

db.persist(g, "cms")

let res = g.v({firstName: "Carl"}).relationOutOf("_type", "parentOf").filter(node => node.firstName == "Lena").relationOutOf("_type", "parentOf").run()

console.log(res)